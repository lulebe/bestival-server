var app = require('express')();
var bodyparser = require('body-parser');
var redis = require('redis').createClient();




//middleware: bodyparser and allow cross-origin requests
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://127.0.0.1:9000");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Encoding, Authorization");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
    next();
});


//log all parameters of each request for debugging
app.use(function (req,res,next) {
  console.log();
  console.log('----------NEW REQUEST-----------');
  var d = new Date();
  console.log(d.getHours()-1 + ':' + d.getMinutes() + ':' + d.getSeconds());
  console.log(req.method + ' to: ' + req.path);
  console.log('headers');
  console.log(req.headers);
  console.log('body');
  console.log(req.body);
  console.log('query');
  console.log(req.query);
  console.log('params');
  console.log(req.params);
  console.log();
  next();
});

//festival list
var getAllFestivals = require('./festivals')(app, redis);
//lineups route
require('./lineup')(app, redis);
//artist pictures
require('./pics')(app);
//spotify
require('./spotify')(app);



//start server
var port = 9014;
var server = app.listen(port, function() {
    console.log("Server is running on " + port + "!");
});
redis.on('connect', function () {
  console.log('redis is connected!');
});