module.exports = function (app, redis) {
  
  var jsdom = require('jsdom');
  var async = require('async');
  
  //get lineup for multiple festivals
  //name & year in JSON Object, JSON array with all those as body
  app.post('/lineups', function (req, res) {
    if (Object.prototype.toString.call(req.body) !== '[object Array]') {
      if (req.body.name) {
        getFestival(req.body, function (err, festival) {
          res.status(200).send(festival);
        });
      } else
        res.status(400).send();
    } else {
      async.map(req.body, getFestival, function (err, festivals) {
        if (err) return res.status(500).send(err);
        res.status(200).send(festivals);
      });
    }
  });
  
  
  //query = {name: "", year: XXXX}
  function getFestival (query, cb) {
    var key = query.name+' '+query.year;
    redis.hgetall(key, function (err, festival) {
      if (err) return cb(err, null);
      if (!festival) return cb(null, null);
      festival.bands = JSON.parse(festival.bands);
      if (festival.bands.length > 0) {
        return cb(null, festival);
      }
      getActs(festival.link, function (bands) {
        festival.bands = bands;
        festival.bands = JSON.stringify(festival.bands);
        redis.hmset(key, festival);
        festival.bands = JSON.parse(festival.bands);
        festival.identifier = key;
        cb(null, festival);
      });
    });
  }
  
  
  function getActs (festivalUrl, cb) {
    if (!festivalUrl) cb([]);
    jsdom.env(festivalUrl, function (err, window) {
      if (err) {
        window.close();
        cb([]);
      } else {
        var bandArray = [];
        var bands = window.document.querySelectorAll('.lineupguide li');
        for (var i = 0;i<bands.length;i++) {
          bandArray.push(bands.item(i).textContent);
        }
        window.close();
        cb(bandArray);
      }
    });
  }
}