module.exports = function (app, redis) {
  
  var jsdom = require('jsdom');
  var async = require('async');
  var CronJob = require('cron').CronJob;
  require('datejs');
  
  
  
  function getFestivalsPage (page, cb) {
    jsdom.env('https://www.musicfestivaljunkies.com/all-festivals/page/'+page, function (err, window) {
      if (err) {
        console.log(err);
        window.close();
        cb(true);
      } else {
        if (window.document.querySelector('.error404.not-found')) { //last page was reached
          window.close();
          cb(null);
        } else { //page exists
          var festivals = window.document.querySelectorAll('.singlefestlisting');
          var ft, d, festival;
          for (var i = 0; i < festivals.length; i++) {
            ft = festivals.item(i).querySelector('.festivaltitle a');
            d = Date.parse(festivals.item(i).querySelector('.festivalstart').innerHTML);
            festival = {
              name: ft.innerHTML.replace(' '+d.getFullYear(), ''),
              startDate: d,
              year: d.getFullYear(),
              link: ft.href,
              imgLink: festivals.item(i).querySelector('.festivallogo img').src,
              bands: '[]'
            };
            redis.hmset(ft.innerHTML, festival);
          }
          window.close();
          getFestivalsPage (++page, cb); //read next page
        }
      }
    });
  }
  
  
  function getAllFestivalNames () {
    var page = 252;
    getFestivalsPage(page, function (err) {
      if (err) return console.log('error during data download');
    });
  }
  new CronJob('0 3 * * 1', getAllFestivalNames);
  
  
  function getAllFestivals (cb) {
    redis.keys('*', function (err, keys) {
      if (err) return cb(true);
      async.map(keys, function (key, mapcb) {
        redis.hgetall(key, function (err, fest) {
          if (err) return mapcb(err);
          fest.identifier = key;
          fest.bands = JSON.parse(fest.bands);
          mapcb(null, fest);
        });
      }, function (err, festivals) {
        if (err) return cb(true);
        cb(null, festivals);
      });
    });
  }
  
  
  //routes
  
  app.get('/festivals', function (req, res) {
    getAllFestivals(function (err, festivals) {
      if (err) return res.status(500).send();
      res.send(festivals);
    });
  });
}