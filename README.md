# Bestival  - Festivalplaylists Server

This is the nodejs server for Bestival ([Bestival-Main](bitbucket.org/lulebe/bestival)). It serves the actlists for festivals, links to artist pictures and Spotify URIs for the top tracks of each artist.

There are 3 submodules:
- iOS Repo
- android Repo
- nodejs server Repo
