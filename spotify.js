module.exports = function (app) {
  var Spotify = require('spotify-web-api-node');
  var spotifyApi = new Spotify();
  
  
  //get Top Tracks for given artist (?artist=ARTISTNAME)
  app.get('/tracks', function (req, res) {
    if (!req.query.artist) return res.status(400).send();
    spotifyApi.searchArtists(req.query.artist).then(function (artists) {
      var artistId = artists.body.artists.items[0].id;
      spotifyApi.getArtistTopTracks(artistId, 'DE').then(function (tracks) {
        var retArr = [];
        tracks.body.tracks.forEach(function (val) {
          retArr.push({uri: val.uri, name: val.name});
        });
        res.status(200).send(retArr);
      },function (err) {
        res.status(500).send(err);
      });
    }, function (err) {
      res.status(500).send(err);
    });
  });
  
}