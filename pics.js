module.exports = function (app) {
  var request = require('superagent');
  
  //get artist picture with ?artist=ARTISTNAME
  app.get('/pics/artists', function (req, res) {
    request.get('http://api.deezer.com/search/artist?q='+req.query.artist).end(function (err, results) {
      if (!results.body || err) return res.status(500).send(err);
      if (results.body.data && results.body.data[0] && results.body.data[0].picture_big)
        res.send({artist_image: results.body.data[0].picture_big});
      else
        res.status(404).send();
    });
  });
}
